import requests
from bs4 import BeautifulSoup
import csv

url = ""
prefix = 'https://en.wikipedia.org'
with open('file.csv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    line_count = 0
    for row in csv_reader:
        if (row[0]=='Germany'): 
            url = row[1]

url = prefix + url
print(url)
req = requests.get(url)
soup = BeautifulSoup(req.content, 'html.parser').find('table', class_ = 'wikitable sortable').find('tbody')
germanyUniversities = []

for row in soup.findAll('tr')[1:]:
    rowData = []
    rowData.append("Germany")
    rowData.append(url)
    rowData.append(row.find('a')['title'])
    rowData.append(prefix + row.find('a')['href'])
    germanyUniversities.append(rowData)

with open('GermanyUniversities.csv', mode='w') as file:
    writer = csv.writer(file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    for i in germanyUniversities:
        writer.writerow(i)