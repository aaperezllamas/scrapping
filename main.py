import csv


def load_csv(path):
    with open(path, "r") as input_file:
        reader = csv.reader(input_file, delimiter=",")
        matrix = [row for row in reader]
    return matrix


file = load_csv("bad_format.csv")
notas = [int(row[4]) for i, row in enumerate(file) if i != 0]
print(notas)
print(sum(notas) / len(notas))
