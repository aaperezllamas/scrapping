import requests
from bs4 import BeautifulSoup
import csv

data = []
url = "https://en.wikipedia.org/wiki/World_population"
request = requests.get(url)
soup = BeautifulSoup(request.content, "html.parser").find('table',class_="wikitable")

soup = soup.find('tbody')
table = soup.findAll('tr')
#table header
header = []
for h in table[0].findAll('th'):
    header.append(h.getText())
data.append(header)

#table content
for row in table[1:]:
    dataRow = []
    unwanted = row.findAll('span')
    for d in unwanted:
        d.extract()
    for rowcol in row.findAll('td'):
        dataRow.append(rowcol.getText())
    data.append(dataRow)


with open('ContinetPopulation.csv', mode='w') as file:
    writer = csv.writer(file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    for i in data:
        writer.writerow(i)
