import requests
from bs4 import BeautifulSoup
import csv

data = []

def grab_stablished_year(*row):
    if(row[3] is not None):
        url = row[3]
        req = requests.get(url)
        soap = BeautifulSoup(req.content, 'html.parser').find('table', 'infobox vcard').find('tbody').findAll('tr')
        for r in soap:
            th = r.find('th')
            td = r.find('td')
            unwanted = td.findAll('span')
            for d in unwanted:
                d.extract()
            if (th is not None and th.getText()=='Established'):
                data.append([row[2], td.getText().strip()])


        

url = ""
prefix = 'https://en.wikipedia.org'
with open('GermanyUniversities.csv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    line_count = 0
    for row in csv_reader:
        grab_stablished_year(*row)

with open('Establishment.csv', mode='w') as file:
    writer = csv.writer(file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    for i in data:
        writer.writerow(i)


