import requests
import json
from bs4 import BeautifulSoup

import requests

url = "https://en.wikipedia.org/wiki/Comillas_Pontifical_University"
response = requests.get(url)
soup = BeautifulSoup(response.content, "html.parser")
box = soup.find_all("table", class_="infobox vcard")
target = box[0]
body = target.find("tbody")
tr = body.find_all("tr")
data = {}
for row in tr:
    th = row.find("th")
    td = row.find("td")
    if th is not None:
        data[th.get_text()] = td.get_text()
print(data)

with open("dic.json", "w") as file:
    json.dump(data, file)




