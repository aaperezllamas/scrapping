from selenium import webdriver
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common import keys
import ChromeDriver as ChromeDriver
import time
import csv

url = 'https://comparadorofertasenergia.cnmc.es/comparador/index.cfm?js=1&e=N'
url2 = 'https://medium.com/'
postal_code = 28008
driver = ChromeDriver.launch_chromedriver(url, False,"")

input = driver.find_element_by_id('electricidad')
time.sleep(1)
input.click()
input = driver.find_element_by_id('iniciar')
time.sleep(1)
input.click()

#indicamos codigo postal
time.sleep(1)
input_cp = driver.find_element_by_id('cp')
input_cp.send_keys(postal_code)
time.sleep(1)
input = driver.find_element_by_id('seguir')
input.click()

#extraigo enlaces
input = driver.find_element_by_tag_name('tbody')
input = input.find_elements_by_tag_name('tr')

#iterate over different row to extract info
data = []
for element in input[1:]:
    try:
        offer, link = None, None
        #search offer
        for td in element.find_elements_by_tag_name('td'):
            if(td.get_attribute('headers') is not None and td.get_attribute('headers')=='oferta'):
                offer = td.text
                break
        #search link
        for td in element.find_elements_by_tag_name('td'):
            if(td.get_attribute('headers') is not None and td.get_attribute('headers')=='ver'):
                link = td.find_element_by_tag_name('a').get_attribute('href')
                break
        if(offer is not None and link is not None):
            data.append([offer, link])
    except: pass

with open('Electricity.csv', mode='w') as file:
    writer = csv.writer(file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    for i in data:
        writer.writerow(i)

time.sleep(3)

driver.close()
