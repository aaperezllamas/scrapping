from selenium import webdriver
import logging
import time
import sys

logger = logging.getLogger("Selenium")
logger.setLevel(logging.INFO)
console = logging.StreamHandler()
console.setLevel(logging.INFO)
logger.addHandler(console)

def launch_chromedriver(url, headless,*args):
    '''
    GOAL:
    Launch the webdriver with different conditions depending on
    arguments passed.
    ARGUMENTS:
    a) -c <chromedriver-path>: Optional argument. Indicates unusual
    path where the '.exe' can be found. Omit this argument if
    chromedriver is in path.
    b) -p <proxy-address>: Optional argument. Indicates the address
    of the proxy server to connect to. Omit this argument if no
    proxies will be used.
    '''
    logger.info('Received a call to "launch_chromedriver".')
    args = args[0]
    chrome_options = webdriver.ChromeOptions()
    
    if(headless==True):
        chrome_options.add_argument('--headless')

    chrome_options.add_argument('--disable-gpu') # Windows need.
    chrome_options.add_argument('--no-sandbox') # Bypass OS security
    chrome_options.add_argument('start-maximized')
    # Loading proxies.
    if '-p' in args:
        logger.info('Launching chromedriver with proxies.')
        proxy_server = '--proxy-server=' + args[args.index('-p')+1]
        chrome_options.add_argument(proxy_server)
    # Loading Chromedriver path.
    if '-c' in args:
        chromedriver_path = args[args.index('-c')+1]
        logger.info('Chromedriver located in unusual path.')
        logger.debug('New path: %s.', chromedriver_path)
        driver = webdriver.Chrome(
        executable_path=chromedriver_path,
        chrome_options=chrome_options)
    else:
        driver = webdriver.Chrome(chrome_options=chrome_options)
        logger.info('Chromedriver located in path.')
    # Launching
    try:
        driver.get(url)
        #time.sleep(2)
        logger.info('Driver up & running.')
        return(driver)
    except Exception as e:
        logger.error(e, exc_info=True)
        sys.exit()
