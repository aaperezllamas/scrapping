from selenium.webdriver.common import keys
from bs4 import BeautifulSoup
from . import ChromeDriver
import time
import requests
import os

def __getDoc__(url, day, month, year):
    driver = ChromeDriver.launch_chromedriver(url, False, '')
    print('launched')
    input = driver.find_element_by_id("fechaBORME")
    input.send_keys(str(day)+'/'+str(month)+'/'+str(year))

    time.sleep(1)

    send = driver.find_element_by_name('acc')
    send.click()

    time.sleep(1)

    url = 'https://www.boe.es/borme/dias/'+str(year)+'/'+str(month)+'/'+str(day)+'/index.php?s='+'C'
    print (url)
    driver.get(url)

    
    tags = driver.find_element_by_class_name('sumario').find_elements_by_tag_name('ul')
    for ul in tags:
        for element in ul.find_elements_by_tag_name('ul > li.dispo'):
            # extract name
            name = element.find_element_by_tag_name('p').text
            # extract li coitaining links for each document
            links = element.find_elements_by_tag_name('li')
            linksURLs = []
            for link in links:
                fileName = link.find_element_by_tag_name('a').get_attribute('title')
                print(fileName)
                link = link.find_element_by_tag_name('a').get_attribute('href')
                linksURLs.append([fileName, link])
            saveFiles(linksURLs, name)

    time.sleep(3)


def saveFiles(linksURLs, name):
    if(len(name)>15):
        name = name[0:15]
    relativePath = 'brome/'+name
    if not os.path.exists(relativePath):
        os.makedirs(relativePath)
    # PDF
    with open(relativePath+'/'+linksURLs[0][0]+'.pdf', 'wb') as f:
         f.write(requests.get(linksURLs[0][1]).content)
    
    # XML
    xmlPage = requests.get(linksURLs[1][1])
    soup = BeautifulSoup(xmlPage.content, "html.parser")
    xml = requests.get('https://www.boe.es'+soup.find('li', class_ = 'puntoXML').find('a')["href"])
    print(xml.content)
    with open(relativePath+'/'+linksURLs[1][0]+'.html', 'wb') as f:
        print (xml.content)
        f.write(xml.content)
