import requests
from bs4 import BeautifulSoup
import csv

url = "https://en.wikipedia.org/wiki/Lists_of_universities_and_colleges_by_country"
req = requests.get(url)
soup = BeautifulSoup(req.content, "html.parser")
soup = soup.find("div", class_ = "mw-parser-output").find_all("ul", recursive = False)
universities = []
for li in soup:
    for a in li.find_all("a"):
        if(a.has_attr("title") and [a.getText(),a["href"]] not in universities and "List" not in a.getText()):
            universities.append([a.getText(),a["href"]])
print(len(universities))
print("Ireland" in universities)

with open('file.csv', mode='w') as file:
    writer = csv.writer(file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    for i in universities:
        writer.writerow(i)
